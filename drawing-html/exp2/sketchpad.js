var canvas = document.getElementById("sketchpad");
var ctx = canvas.getContext("2d");

var ctx = canvas.getContext("2d");
ctx.fillStyle = "purple";

var brushSize = 5;

function drawDot(x, y) {
    ctx.beginPath();
    ctx.arc(x, y, brushSize, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
}

var isMouseDown = false;

function mouseUp(e) {
    isMouseDown = false;
}

function mouseDown(e) {
    isMouseDown = true;
    drawDot(mouseX, mouseY);
}

function mouseMove(e) {
    let {mouseX, mouseY} = getMousePos(e);
    if (isMouseDown) {
        drawDot(mouseX, mouseY);
    }
}

function getMousePos(e) {
    if (e.offsetX) {
        mouseX = e.offsetX;
        mouseY = e.offsetY;
    } else if (e.layerX) {
        mouseX = e.layerX;
        mouseY = e.layerY;
    }
    return {mouseX, mouseY};
}

window.addEventListener("mouseup", mouseUp, false);
canvas.addEventListener("mousedown", mouseDown, false);
canvas.addEventListener("mousemove", mouseMove, false);

function touchStart(e) {
    let {touchX, touchY} = getTouchPos();
    drawDot(touchX, touchY);
    e.preventDefault();
}

function touchMove(e) {
    let {touchX, touchY} = getTouchPos(e);
    drawDot(touchX, touchY);
    e.preventDefault();
}

function getTouchPos(e) {
    if (e.touches && e.touches.length == 1) {
        var touch = e.touches[0];
        touchX = touch.pageX-touch.target.offsetLeft;
        touchY = touch.pageY-touch.target.offsetTop;
        return {touchX, touchY};
    }
}

canvas.addEventListener("touchstart", touchStart, false);
canvas.addEventListener("touchmove", touchMove, false);
