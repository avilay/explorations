var canvas = document.getElementById("sketchpad");
var ctx = canvas.getContext("2d");
ctx.fillStyle = "pink";

function drawDot(x, y, brushSize) {
    ctx.beginPath();
    ctx.arc(x, y, brushSize, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
}

drawDot(100, 100, 5);

// function drawDot(x, y, size) {
//     var ctx = canvas.getContext("2d");
//     ctx.fillStyle = "pink";
//     ctx.beginPath();
//     ctx.arc(x, y, size, 0, Math.PI*2, true);
//     ctx.closePath();
//     ctx.fill;
// }

// drawDot(100, 100, 10);